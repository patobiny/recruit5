@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "summary">Interview summmary</label>
            <input type = "text" class="form-control" name = "summary">
        </div>  
        <div class="form-group">
            <label for = "date">Interview date</label>
            <input type = "text" class="form-control" name = "date">
        </div>    
        <label for="candidate_id" class="col-md-4 col-form-label text-md-right">Choose Candidate</label>
        <div class="col-md-6">
                        <select class="form-control" name="candidate_id">                                                                         
                          @foreach ($candidates as $candidate)
                          <option value="{{ $candidate->id }}"> 
                              {{ $candidate->name }} 
                          </option>
                          @endforeach    
                        </select>
                    
                            
                    </div>
         
        <div>
        <label for="user_id" class="col-md-4 col-form-label text-md-right">Choose Users</label>
        <div class="col-md-6">
                        <select class="form-control" name="user_id">                                                                         
                          @foreach ($users as $user)
                          <option value="{{ $user->id }}"> 
                              {{ $user->name }} 
                          </option>
                          @endforeach    
                        </select>
                    
                            
                    </div>
         
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
