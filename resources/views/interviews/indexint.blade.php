@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div> <a class="navbar-brand" href="{{ route('interviews.create') }}"> Add new Interview</a></div>
<h1>List of Interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Summary</th><th>Date</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->summary}}</td>
            <td>{{$interview->date}}</td>
          
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($interview->candidate_id))
                          {{$interview->candidates->name}}  
                        @else
                          Assign Candidate
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                      <a class="dropdown-item" href="{{route('interview.changecandidate',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td> 
           <td>
            @if(isset($interview->user_id))
                {{$interview->users->name}}  
            @else
                no Interviewer yet
            @endif
            </td>

                             
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
            
    @endforeach
</table>
@endsection

